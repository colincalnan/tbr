'use strict';

/**
 * @ngdoc directive
 * @name guidepostApp.directive:noContextMenu
 * @description
 * # noContextMenu
 */
angular.module('guidepostApp')
  .directive('noContextMenu', function ($location) {
    return function(scope, element) {
      element.on('contextmenu', function() {
        if($location.host() !== '0.0.0.0') {
          return false;
        }
      });
    };
  });
