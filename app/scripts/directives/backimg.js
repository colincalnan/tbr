'use strict';

/**
 * @ngdoc directive
 * @name guidepostApp.directive:backImg
 * @description
 * # backImg
 */
angular.module('guidepostApp')
  .directive('backImg', function () {
    return function(scope, element, attrs){
      attrs.$observe('backImg', function(value) {
        element.css({
          'background-image': 'url(' + value +')'
        });
      });
    };
  });
