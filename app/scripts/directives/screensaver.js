'use strict';

/**
 * @ngdoc directive
 * @name guidepostApp.directive:screensaver
 * @description
 * # screensaver
 */
angular.module('guidepostApp')
  .directive('screensaver', function ($timeout, $location, $state) {
    return function(scope, element) {

      // Prevent screensaver on localhost
      if($location.host() === '0.0.0.0') {
        return;
      }

      var sleep = function() {
        $state.go('home');
      };

      var screensaverTimeout = 120000; // 2 min.
      var sleeper = $timeout(sleep, screensaverTimeout);

      element.on('touchstart click', function() {
        $timeout.cancel(sleeper);
        sleeper = $timeout(sleep, screensaverTimeout);
      });
    };
  });
