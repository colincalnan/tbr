'use strict';

/**
 * @ngdoc function
 * @name guidepostApp.controller:EventListCtrl
 * @description
 * # EventListCtrl
 * Controller of the guidepostApp
 */
angular.module('guidepostApp')
  .controller('EventListCtrl', function ($scope, event) {

    $scope.event = event;

  });
