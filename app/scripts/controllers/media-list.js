'use strict';

/**
 * @ngdoc function
 * @name guidepostApp.controller:MedialistCtrl
 * @description
 * # MedialistCtrl
 * Controller of the guidepostApp
 */
angular.module('guidepostApp')
  .controller('MediaListCtrl', function ($scope, media) {
    $scope.media = media;
  });
