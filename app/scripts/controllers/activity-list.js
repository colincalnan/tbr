'use strict';

/**
 * @ngdoc function
 * @name guidepostApp.controller:ActivitiesCtrl
 * @description
 * # ActivitiesCtrl
 * Controller of the guidepostApp
 */
angular.module('guidepostApp')
  .controller('ActivityListCtrl', function ($scope, activity, modal, $state) {

    $scope.activity = activity;

    $scope.showType = function(name, list) {

      // List activities or go directly to the activity
      // if only one

      if(list.length > 1) {

        modal.obj = {
          name: name,
          list: list
        };

        modal.open('activity_group_list.html');
      } else {

        $state.go('activity_detail', {activityId: list[0].id});
      }

    };


  });
