'use strict';

/**
 * @ngdoc function
 * @name guidepostApp.controller:ModalCtrl
 * @description
 * # ModalCtrl
 * Controller of the guidepostApp
 */
angular.module('guidepostApp')
  .controller('ModalCtrl', function ($scope, modal) {

    $scope.modal = modal;
  });
