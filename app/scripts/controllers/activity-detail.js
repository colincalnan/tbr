'use strict';

/**
 * @ngdoc function
 * @name guidepostApp.controller:ActivitiesCtrl
 * @description
 * # ActivitiesCtrl
 * Controller of the guidepostApp
 */
angular.module('guidepostApp')
  .controller('ActivityDetailCtrl', function ($scope, $stateParams, activity, Restangular, modal) {

    $scope.modal = modal;

    var _media = {
      index: 0
    };

    $scope._media = _media;

    $scope.videoParams = {
      controls: 0,
      rel: 0
    };

    // Load activity
    Restangular.one('activity', $stateParams.activityId).get().then(function(data) {
      $scope.activity = data;
    });

    $scope.packageMore = function(_package) {
      modal.obj = _package;
      modal.open('package_detail.html');
    };

    // Gopro
    $scope.openGoproModal = function() {
      modal.open('gopro.html');
    };

  });
