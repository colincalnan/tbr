'use strict';

/**
 * @ngdoc function
 * @name guidepostApp.controller:MediadetailCtrl
 * @description
 * # MediadetailCtrl
 * Controller of the guidepostApp
 */
angular.module('guidepostApp')
  .controller('MediaDetailCtrl', function ($scope, Restangular, $stateParams) {
    $scope.media = Restangular.one('media', $stateParams.mediaId).get().$object;

    $scope.videoParams = {
      controls: 0,
      rel: 0
    };

  });
