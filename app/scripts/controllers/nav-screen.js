'use strict';

/**
 * @ngdoc function
 * @name guidepostApp.controller:NavScreenCtrl
 * @description
 * # NavScreenCtrl
 * Controller of the guidepostApp
 */
angular.module('guidepostApp')
  .controller('NavScreenCtrl', function ($scope, section, $state) {
    $scope.section = section;

    $scope.isStateActive = function(state) {
      return $state.current.nav === state;
    };

  });
