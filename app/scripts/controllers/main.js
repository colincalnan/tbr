'use strict';

/**
 * @ngdoc function
 * @name guidepostApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the guidepostApp
 */
angular.module('guidepostApp')
  .controller('MainCtrl', function () {

    // Angular Carousel 0.2.5 auto-slide does NOT work with ng-repeat,
    // see: https://github.com/revolunet/angular-carousel/issues/184

    // Angular Carousel 0.3.1 overwrites inline styles, and therefore prevent
    // the 'back-img' directive from working

    // var screensaver = {};

    // Restangular.all('screensaver').getList().then(function(data) {
    //   screensaver.objects = data;
    // });

    // $scope.screensaver = screensaver;

  });
