'use strict';

/**
 * @ngdoc function
 * @name guidepostApp.controller:EventListCtrl
 * @description
 * # EventListCtrl
 * Controller of the guidepostApp
 */
angular.module('guidepostApp')
  .controller('InstagramListCtrl', function ($scope, $http, modal, config) {

    // TBR Feed
    // var apiUrl = 'https://api.instagram.com/v1/users/15560501/media/recent/';
    // By keyword
    var apiUrl = 'https://api.instagram.com/v1/tags/turtlebayresort/media/recent/';

    var imageCount = 26;
    if(config.device === 'ipad') {
      imageCount = 25;
    }

    $http.jsonp(apiUrl, {
      /*jshint camelcase: false */
      params: {
        client_id: '247a0b1971f64425bc46c51793bcc384',
        count: imageCount,
        callback: 'JSON_CALLBACK'
      }
    }).success(function(data) {
      $scope.instagramFeed = data;
    });

    // Zoom
    $scope.showDetail = function(picture) {

      modal.obj = {
        picture: picture.images.standard_resolution
      };

      // Caption can be empty
      if(picture.caption) {
        modal.obj.caption = picture.caption.text;
        modal.obj.username = picture.caption.from.username;
      }

      modal.open('instagram_detail.html');
    };

    // Highlight
    $scope.isItemLarge = function(index) {
      if(config.device === 'ipad') {
        if(index % 5 === 0) {
          return true;
        }
      } else {
        if(index === 0 || index === 7 || index === 14) {
          return true;
        }
      }
      return false;
    };

    $scope.isItemRight = function(index) {
      if(config.device === 'ipad') {
        if(index % 10 === 0) {
          return true;
        }
      } else {
        if(index % 14 === 0) {
          return true;
        }
      }
      return false;
    };

  });
