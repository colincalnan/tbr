'use strict';

/**
 * @ngdoc function
 * @name guidepostApp.controller:SatelliteMapCtrl
 * @description
 * # SatelliteMapCtrl
 * Controller of the guidepostApp
 */
angular.module('guidepostApp')
  .controller('SatelliteMapCtrl', function ($scope, $state, marker, map, config) {

    $scope.marker = marker;

    // Device based zoom
    var mapConfig = {
      minZoom: 11,
      maxZoom: 18,
      zoom: 11
    };
    if(config.device === 'ipad') {
      mapConfig = {
        minZoom: 10,
        maxZoom: 18,
        zoom: 10
      };
    }

  	$scope.map = {
      defaults: angular.extend({}, map.base.defaults, {
        minZoom: mapConfig.minZoom,
        maxZoom: mapConfig.maxZoom,
      }),
  		center: {
  			lat: 21.4,
  			lng: -157.95,
        zoom: mapConfig.zoom
  		},
      layers: {
        baselayers: {
          mapbox: {
            name: 'Mapbox',
            type: 'xyz',
            url: 'https://{s}.tiles.mapbox.com/v3/jeremysd.jjj5e3on/{z}/{x}/{y}.png'
          },
          // osm: {
          //   name: 'OpenStreetMap',
          //   type: 'xyz',
          //   url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
          //   layerOptions: {
          //     subdomains: ['a', 'b', 'c'],
          //     attribution: '© OpenStreetMap contributors',
          //     continuousWorld: true
          //   }
          // }
        },
        overlays: map.base.layers.overlays
      }
  	};

    // Go to activity on marker click
    $scope.$on('leafletDirectiveMarker.click', function(e, args) {
      var activityId = marker.markers[args.markerName].activityId;
      if(activityId) {
        $state.go('activity_detail', {activityId: activityId});
      }
    });

    // Zoom in/out
    $scope._map = map;

  });
