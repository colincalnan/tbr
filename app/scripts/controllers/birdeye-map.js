'use strict';

/**
 * @ngdoc function
 * @name guidepostApp.controller:BirdeyeMapCtrl
 * @description
 * # BirdeyeMapCtrl
 * Controller of the guidepostApp
 */
angular.module('guidepostApp')
  .controller('BirdeyeMapCtrl', function ($scope, marker, leafletData, $state, map, config) {

    $scope.marker = marker;

    // Device based zoom
    var mapConfig = {
      minZoom: 16,
      maxZoom: 18,
      zoom: 17
    };
    if(config.device === 'ipad') {
      mapConfig = {
        minZoom: 16,
        maxZoom: 18,
        zoom: 16
      };
    }

    // Based on the dimensions of the largest map
    var _southWest = [0, 4608];
    var _northEast = [6656, 0];

    leafletData.getMap().then(function(map) {
      var southWest = map.unproject(_southWest, map.getMaxZoom());
      var northEast = map.unproject(_northEast, map.getMaxZoom());
      var bounds = new L.LatLngBounds(southWest, northEast);
      map.setMaxBounds(bounds);
      // map.setView(bounds.getCenter());
    });

    $scope.map = {
      defaults: angular.extend({}, map.base.defaults, {
        crs: 'Simple',
        minZoom: mapConfig.minZoom,
        maxZoom: mapConfig.maxZoom,
      }),
      center: {
        lat: -0.01143646240234375,
        lng: 0.0139312744140625,
        zoom: mapConfig.zoom
      },
      layers: {
        baselayers: {
          island: {
            name: 'Image',
            type: 'xyz',
            url: 'images/tiles/{z}/{x}/{y}.jpg',
            layerParams: {
              noWrap: true
            }
          }
        },
        overlays: map.base.layers.overlays
      }
    };

    // Helper to locate points
    $scope.$on('leafletDirectiveMap.click', function(e, args) {
      var latlng = args.leafletEvent.latlng;
      console.log('%s,%s', latlng.lat, latlng.lng);
    });

    // Go to activity on marker click
    $scope.$on('leafletDirectiveMarker.click', function(e, args) {
      var activityId = marker.imageMarkers[args.markerName].activityId;
      if(activityId) {
        $state.go('activity_detail', {activityId: activityId});
      }
    });

    // Zoom in/out
    $scope._map = map;

  });
