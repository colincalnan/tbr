'use strict';

/**
 * @ngdoc service
 * @name guidepostApp.map
 * @description
 * # map
 * Factory in the guidepostApp.
 */
angular.module('guidepostApp')
  .factory('map', function (leafletData) {

    // Shared settings between maps

    var map = {
      base: {
        defaults: {
          attributionControl: false,
          zoomControl: false,
          controls: {
            layers: {
              visible: false
            }
          }
        },
        layers: {
          overlays: {
            activities: {
              name: 'Activities',
              type: 'markercluster',
              visible: true,
              layerParams: {
                showCoverageOnHover: false
              }
            }
          }
        }
      }
    };

    // Zoom in
    map.zoomIn = function() {
      leafletData.getMap().then(function(map) {
        map.zoomIn();
      });
    };

    // Zoom out
    map.zoomOut = function() {
      leafletData.getMap().then(function(map) {
        map.zoomOut();
      });
    };

    return map;
  });
