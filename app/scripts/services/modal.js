'use strict';

/**
 * @ngdoc service
 * @name guidepostApp.modal
 * @description
 * # modal
 * Factory in the guidepostApp.
 */
angular.module('guidepostApp')
  .factory('modal', function ($rootScope) {

    var modal = {
      open: function(template) {
        this.active = true;
        this.template = 'views/modal_' + template;
      },

      close: function() {
        this.active = false;
        // Do not reset template, or else the fading out animation doesn't make sense
        //this.template = '';
      },

      isActive: function() {
        return this.active;
      }
    };

    // Events
    $rootScope.$on('$stateChangeStart', function() {
      modal.close();
    });

    return modal;
  });
