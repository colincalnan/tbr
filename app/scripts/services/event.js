'use strict';

/**
 * @ngdoc service
 * @name guidepostApp.events
 * @description
 * # events
 * Factory in the guidepostApp.
 */
angular.module('guidepostApp')
  .factory('event', function (Restangular) {

    var event = {};

    // Load events
    var _Event = Restangular.all('event');

    _Event.getList().then(function(data) {
      event.objects = data;
    });

    return event;
  });
