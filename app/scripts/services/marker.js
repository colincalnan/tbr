'use strict';

/**
 * @ngdoc service
 * @name guidepostApp.marker
 * @description
 * # marker
 * Factory in the guidepostApp.
 */
angular.module('guidepostApp')
  .factory('marker', function (Restangular) {

    var marker = {};

    // Load markers
    var _Marker = Restangular.all('marker');

    _Marker.getList().then(function(data) {
      marker.objects = data;
      var assignedMarkers = marker.assignMarkers();
      marker.markers = assignedMarkers[0];
      marker.imageMarkers = assignedMarkers[1];
    });

    // Assign markers
    marker.assignMarkers = function () {
      /*jshint camelcase: false */

      // TODO: refactor

      var markers = [];
      var imageMarkers = [];

      angular.forEach(this.objects, function(marker) {

        var activityId = null;
        if(marker.activity) {
          activityId = marker.activity.id;
        }

        var markerData = {
          layer: 'activities',
          activityId: activityId,
          label: {
            message: marker.name,
            options: {
              noHide: true,
            }
          }
        };

        if(marker.icon) {
          markerData.icon = {
            iconUrl: marker.icon,
            iconSize: [32, 48],
            iconAnchor: [16, 48],
            labelAnchor:  [10, -20]
          };
        }

        // GPS markers
        if(marker.latitude && marker.longitude) {
          var newMarker = Object.create(markerData);
          newMarker.lat = parseFloat(marker.latitude);
          newMarker.lng = parseFloat(marker.longitude);
          markers.push(newMarker);
        }

        // Image markers
        if(marker.image_latitude && marker.image_longitude) {
          var newImageMarker = Object.create(markerData);
          newImageMarker.lat = parseFloat(marker.image_latitude);
          newImageMarker.lng = parseFloat(marker.image_longitude);
          imageMarkers.push(newImageMarker);
        }
      });

      return [markers, imageMarkers];
    };

    return marker;

  });
