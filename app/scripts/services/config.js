'use strict';

/**
 * @ngdoc service
 * @name guidepostApp.config
 * @description
 * # config
 * Factory in the guidepostApp.
 */
angular.module('guidepostApp')
  .factory('config', function ($rootScope, $location) {

    var config = {
      baseUrl: 'http://replay.storydriven.com/tbr/',
      apiUrl: 'http://replay.storydriven.com/tbr/api/v1',
      device: 'ts' // "ts" is for "touchscreen"
      // Comment previous line and uncomment next line to force device mode
      // device: 'ipad'
    };

    // Rely on the navigator object for iPad detection.
    // Since this can be faked, this is NOT bulletproof,
    // but we don't expect anyone to change the navigator object
    // on the production iPads
    if(navigator.platform === 'iPad') {
      config.device = 'ipad';
    }

    // Dev
    // var dev = true;
    var dev = false;

    if(dev && $location.host() === '0.0.0.0') {
      config = {
        baseUrl: 'http://angryj:8000/tbr/',
        apiUrl: 'http://angryj:8000/tbr/api/v1',
      };
    }

    $rootScope.config = config;

    return config;
  });
