'use strict';

/**
 * @ngdoc service
 * @name guidepostApp.media
 * @description
 * # media
 * Factory in the guidepostApp.
 */
angular.module('guidepostApp')
  .factory('media', function (Restangular) {

    var media = {};

    var _Media = Restangular.all('media');

    _Media.getList().then(function(data) {
      media.objects = data;
      media.photos = _.where(data, {type: 'image'});
      media.videos = _.where(data, {type: 'video'});
    });

    return media;
  });
