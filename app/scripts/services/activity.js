'use strict';

/**
 * @ngdoc service
 * @name guidepostApp.activities
 * @description
 * # activities
 * Factory in the guidepostApp.
 */
angular.module('guidepostApp')
  .factory('activity', function (Restangular) {

    var activity = {};

    // Load activities
    var _Activity = Restangular.all('activity');

    _Activity.getList().then(function(data) {
      activity.objects = data;

      activity.onSite = _.filter(data, {'on_site': true});
      activity.offSite = _.filter(data, {'on_site': false});

      // Group activities by activity group
      // Prepending the group order allows the ordering to be respected
      // This behavior seems unreliable: https://code.google.com/p/v8/issues/detail?id=164
      activity.onSite = _.groupBy(activity.onSite, function(item) {
        return item.group.order + ' ' + item.group.name;
      });

      activity.offSite = _.groupBy(activity.offSite, function(item) {
        return item.group.order + ' ' + item.group.name;
      });

      activity.groups = [{
        name: 'Play at Turtle Bay',
        slug: 'resort',
        list: activity.onSite
      }, {
        name: 'Explore the North Shore & Oahu',
        slug: 'north-shore',
        list: activity.offSite
      }];

    });

    return activity;
  });
