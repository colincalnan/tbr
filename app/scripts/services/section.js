'use strict';

/**
 * @ngdoc service
 * @name guidepostApp.section
 * @description
 * # section
 * Factory in the guidepostApp.
 */
angular.module('guidepostApp')
  .factory('section', function ($rootScope) {

    return {

      setActive: function(section) {
        $rootScope.$broadcast('selecting', section);
        this.active = section;
        $rootScope.$broadcast('selected', section);
      },

      isActive: function(section) {
        return this.active === section;
      }
    };
  });
