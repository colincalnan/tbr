'use strict';

/**
 * @ngdoc overview
 * @name guidepostApp
 * @description
 * # guidepostApp
 *
 * Main module of the application.
 */

angular
  .module('guidepostApp', [
    'ngAnimate',
    'ui.router',
    'ngSanitize',
    'ngTouch',
    'restangular',
    'angular-carousel',
    'leaflet-directive',
    'youtube-embed'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'views/screensaver.html',
        controller: 'MainCtrl'
      })
      .state('activity_list', {
        url: '/activities',
        templateUrl: 'views/activity_list.html',
        controller: 'ActivityListCtrl',
        nav: 'activity'
      })
      .state('activity_detail', {
        url: '/activities/:activityId',
        templateUrl: 'views/activity_detail.html',
        controller: 'ActivityDetailCtrl',
        nav: 'activity'
      })
      .state('media_list', {
        url: '/gallery',
        templateUrl: 'views/media_list.html',
        controller: 'MediaListCtrl',
        nav: 'gallery',
      })
      .state('media_detail', {
        url: '/gallery/:mediaId',
        templateUrl: 'views/media_detail.html',
        controller: 'MediaDetailCtrl',
        nav: 'gallery'
      })
      .state('satellite_map', {
        url: '/satellite-map',
        templateUrl: 'views/satellite_map.html',
        controller: 'SatelliteMapCtrl',
        nav: 'satellite-map'
      })
      .state('birdeye_map', {
        url: '/birdeye-map',
        templateUrl: 'views/birdeye_map.html',
        controller: 'BirdeyeMapCtrl',
        nav: 'birdeye-map'
      })
      .state('event_list', {
        url: '/events',
        templateUrl: 'views/event_list.html',
        controller: 'EventListCtrl',
        nav: 'events'
      })
      .state('instagram_list', {
        url: '/instagram',
        templateUrl: 'views/instagram_list.html',
        controller: 'InstagramListCtrl',
        nav: 'instagram'
      });
  })
  .config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
      'self',
      'http://localhost:8000**',
      'http://192.168.1.150:8000**',
      'http://replay.storydriven.com**'
    ]);
  })
  .config(function(RestangularProvider) {
    RestangularProvider.setResponseExtractor(function(response, operation) {
      // Adjust to Tastypie response format
      var newResponse;
      if (operation === 'getList') {
        newResponse = response.objects;
        newResponse.metadata = response.meta;
      } else {
        newResponse = response;
      }
      return newResponse;
    });
  })
  .run(function(Restangular, config) {
    Restangular.setBaseUrl(config.apiUrl);
  });
