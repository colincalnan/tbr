'use strict';

describe('Controller: ResortMapCtrl', function () {

  // load the controller's module
  beforeEach(module('guidepostApp'));

  var ResortMapCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ResortMapCtrl = $controller('ResortMapCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
