'use strict';

describe('Controller: NavScreenCtrl', function () {

  // load the controller's module
  beforeEach(module('guidepostApp'));

  var NavScreenCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NavScreenCtrl = $controller('NavScreenCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
