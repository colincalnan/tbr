'use strict';

describe('Controller: MediadetailCtrl', function () {

  // load the controller's module
  beforeEach(module('guidepostApp'));

  var MediadetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MediadetailCtrl = $controller('MediadetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
