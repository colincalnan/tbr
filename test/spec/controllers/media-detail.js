'use strict';

describe('Controller: MediaDetailCtrl', function () {

  // load the controller's module
  beforeEach(module('guidepostApp'));

  var MediaDetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MediaDetailCtrl = $controller('MediaDetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
