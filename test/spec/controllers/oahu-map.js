'use strict';

describe('Controller: OahuMapCtrl', function () {

  // load the controller's module
  beforeEach(module('guidepostApp'));

  var OahuMapCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OahuMapCtrl = $controller('OahuMapCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
