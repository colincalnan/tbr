'use strict';

describe('Service: activities', function () {

  // load the service's module
  beforeEach(module('guidepostApp'));

  // instantiate service
  var activities;
  beforeEach(inject(function (_activities_) {
    activities = _activities_;
  }));

  it('should do something', function () {
    expect(!!activities).toBe(true);
  });

});
