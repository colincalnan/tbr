'use strict';

describe('Directive: noContextMenu', function () {

  // load the directive's module
  beforeEach(module('guidepostApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<no-context-menu></no-context-menu>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the noContextMenu directive');
  }));
});
