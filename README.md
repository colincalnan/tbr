# README #

### How do I get set up? ###

* Clone the repository via SSH
* Run 'npm install' to install node dependencies
* Run 'bower install' to install bower components
* Run 'grunt serve' to launch local instance of the app

You're ready to code!